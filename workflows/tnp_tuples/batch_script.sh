#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

baseline='doEleID=True doPhoID=True doTrigger=True'
isMC='isMC=False'
era='era=2022' #TnPProducer to be modified to load GT rather than era 
lumimask='LUMIJSON=/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_355100_362760_Golden.json' #to be checked if can be taken from CAF
numEvents='maxEvents=-1'
outFile='OUTPUT_FILE_NAME=TnPTree_'$JOBID'.root'


trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

mkdir output/

export PYTHON3PATH=$PYTHON3PATH:$(pwd)
export PYTHONPATH=$PYTHONPATH:$(pwd)


cmsRun TnPTreeProducer_cfg.py $baseline $isMC $era $lumimask $maxEvents $outFile INPUTDATASET=$INFILE
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    mkdir -p $EOSDIR
    cp TnPTree_$JOBID.root $EOSDIR/. 
    rm *root
    OFILE=$EOSDIR/TnPTree_$JOBID.root
    RETCOPY=$?
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET

#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandlerByRunDBS, CondDBLockGT, T0ProcDatasetLock
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

if __name__ == '__main__':
    
    handler = HTCHandlerByRunDBS(task='tnp-ntuples',
                                 dsetname='/EGamma*/*-PromptReco-*/MINIAOD',
                               )

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerByRunDBS)
    

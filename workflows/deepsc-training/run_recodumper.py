#!/usr/bin/env python3
import sys
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase, process_by_run, HTCHandler
from ecalautoctrl.CMSTools import QueryDBS


import argparse
import subprocess
import logging
import inspect
from datetime import datetime
from typing import Optional, List, Dict, Type, Union
from os import path
from itertools import islice
from functools import partialmethod



def get_files_dbs_user(self, runs: List[Dict], dbs_instance: str) -> List[str]:
    """
    Retrieve file names for given runs from DBS.
    This function is added by :deco:`~ecalautoctrl.dbs_data_source`.

    :param runs: list of input runs dictionaries (from :meth:`~ecalautoctrl.RunCtrl.getRuns`).
    """
    flist = []
    try:
        if self.dsetname:
            dsets = [self.dsetname] if isinstance(self.dsetname, str) else self.dsetname
            for dset in dsets:
                for run in runs:
                    # add era to dataset name, validating it in the process
                    _, pd, sd, tier = dset.split('/')
                    sd = '*' + run["era"] + sd
                    fullname = f'/{pd}/{sd}/{tier}'
                    fromt0 = self.opts.t0 if 't0' in self.opts else False
                    dataQuery = QueryDBS(dataset=fullname, dbs_instance=dbs_instance)
                    flist.extend(dataQuery.getRunFiles(run=int(run['run_number']), fromt0=fromt0))

        return flist
    except AttributeError:
        raise AttributeError('Please define self.dsetname in your class when using the dbs_data_source decorator')


# Decorators
def dbs_data_source_user(dbs_instance='https://cmsweb.cern.ch/dbs/prod/phys03/DBSReader'):
    """
    This decorator adds the a :func:`get_files` function that fetches data from dbs.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    The decorator requires the data member :attr:`dsetname` to be defined in the class.
    Usually this has to be set in the derived class :meth:`__init__`.
    """
    def wrapper(cls):    
        setattr(cls, 'get_files',partialmethod(get_files_dbs_user, dbs_instance=dbs_instance))
        return cls
    return wrapper


@dbs_data_source_user(dbs_instance='https://cmsweb.cern.ch/dbs/prod/phys03/DBSReader')
@process_by_run
class HTCHandlerByRunDBSUser(HTCHandler):
    """
    HTCHandler to process data run by run, reading data from DBS.
    The acquisition era is automatically adjusted based on the run number in
    the dataset name.
    
    :param task: task name.
    :param deps_tasks: list of workfow dependencies.
    :param dsetname: central dataset name(s) in the three fields format (/prim/sec/tier, wildcards allowd)
    """

    def __init__(self,
                 task: str,
                 dsetname: Union[str, List[str]],
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.dsetname = dsetname


                


if __name__ == '__main__':
    
    handler = HTCHandlerByRunDBSUser(task='deepsc-dump-from-raw',
                                 dsetname='/FourElectronsGunPt1-500_PremixRun3_13p6TeV_126X_mcRun3_2023_forPU65_v4_Automation/*-CALO-06da012d8ffef3294f8545e15a2ae64e/USER',
                               )

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerByRunDBS)
    

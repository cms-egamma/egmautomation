#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

mkdir output/

export PYTHON3PATH=$PYTHON3PATH:$(pwd)
export PYTHONPATH=$PYTHONPATH:$(pwd)


cmsDriver.py --python_filename RecoSimDumper_fromRAW_Mustache_StdMC_Training_cfg.py \
             --eventcontent MINIAODSIM --datatier MINIAODSIM --fileout file:step3.root \
             --conditions  ${GT} \
             --step RAW2DIGI,L1Reco,RECO,RECOSIM,PAT --geometry DB:Extended \
             --filein=$INFILE \
             --era Run3  --mc \
             --customise RecoSimStudies/Dumpers/customizers.dumperStepMC_training \
             --processName ECALClustering \
             --customise_commands 'from RecoEgamma.EgammaElectronProducers.gsfElectrons_cfi import * \n from RecoEgamma.EgammaPhotonProducers.gedPhotons_cfi import *'\
             -n -1 --no_exec \
             --nThreads=2 --nStreams=2 --nConcurrentLumis=2 \


cmsRun -n 2 RecoSimDumper_fromRAW_Mustache_StdMC_Training_cfg.py
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    mkdir -p $EOSDIR
    cp ecal_phisym_nano.root $EOSDIR/phisymreco_nano_$JOBID.root
    rm *root
    OFILE=$EOSDIR/phisymreco_nano_$JOBID.root
    RETCOPY=$?
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET

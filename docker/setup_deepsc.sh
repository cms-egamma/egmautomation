#!/bin/bash


## Copy DeepSuperCluster files  
#   keeping only the necessary scripts
cd /home/ecalgit

mkdir deepSC_code
cd deepSC_code
git clone https://github.com/valsdav/DeepSuperCluster.git deepSC_repo
mkdir truth_studies
mkdir training_ntuples

cd deepSC_repo/NtuplesProduction/
cp windows_creator_general.py ../../training_ntuples


rm -rf /home/ecalgit/deepSC_code/deepSC_repo